#ifndef FILES
#define FILES
#include <stdbool.h>
#include <stdio.h>

bool open_file(FILE** file, const char* name, const char* mode);
int close_file(FILE** file);
#endif
