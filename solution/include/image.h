#ifndef IMAGE
#define IMAGE
#include <stdbool.h>
#include <stdint.h>
#pragma pack(push, 1)
struct pixel { 
	uint8_t b, g, r; 
};
#pragma pack(pop)
#pragma pack(push, 1)
struct image {
	uint32_t width, height;
	struct pixel* data;
};
#pragma pack(pop)
struct image image_create(uint32_t width, uint32_t height);

void image_destroy(struct image *img);

#endif
