#include "image.h"
#include <malloc.h>


struct image image_create(uint32_t width, uint32_t height) {
	struct image img = {
	  .width = width,
	  .height = height,
	  .data = malloc(sizeof(struct pixel) * width * height)
	};
	return img;
}


void image_destroy(struct image *img) {
	free(img->data);
}
