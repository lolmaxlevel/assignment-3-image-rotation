#include "files.h"

bool open_file(FILE** file, const char* name, const char* mode) {
	*file = fopen(name, mode);
	if (*file != NULL) {
		return true;
	}
	return false;
}

int close_file(FILE** file) {
	return fclose(*file);
}
