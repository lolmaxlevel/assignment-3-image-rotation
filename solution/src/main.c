#include "files.h"
#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>

void close_files_n_clear_mem(FILE *in, FILE *out, struct image *img){
    fclose(in);
    fclose(out);
    image_destroy(img);
}

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "wrong amount of args passed(should be 3)\n");
        return -1;
    }
    FILE* in = NULL;
    FILE* out = NULL;

    if (!open_file(&in, argv[1], "rb")) {
        fprintf(stderr, "can't open file for reading\n");
        return -1;
    }
    struct image img = { 0 };

    switch (from_bmp(in, &img)) {
        case READ_OK:
            printf("bmp opened successfully\n");
            break;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "bad file type\n");
            close_files_n_clear_mem(in, out, &img);
            return -1;
        case READ_INVALID_BITS:
            fprintf(stderr, "bad bitcount\n");
            close_files_n_clear_mem(in, out, &img);
            return -1;
        case READ_INVALID_HEADER:
            fprintf(stderr, "can't read header\n");
            close_files_n_clear_mem(in, out, &img);
            return -1;
        case NOT_ENOUGH_MEMORY:
            fprintf(stderr, "can't allocate memory for image\n");
            close_files_n_clear_mem(in, out, &img);
            return -1;
        case READ_BAD_DATA:
            fprintf(stderr, "bad data inside\n");
            close_files_n_clear_mem(in, out, &img);
            return -1;
    }

    if (close_file(&in) == EOF) {
       fprintf(stdout, "error occurred, exiting\n");
       fprintf(stderr, "can't close file\n");
        close_files_n_clear_mem(in, out, &img);
        return -1;
    }
    printf("file closed\n");
    struct image rotated_img = rotate(img);
    printf("image rotated\n");
    image_destroy(&img);
    printf("memory freed\n");

    if (!open_file(&out, argv[2], "wb")) {
        fprintf(stderr, "can't open file for writing\n");
        close_files_n_clear_mem(in, out, &rotated_img);
        return -1;
    }

    if (to_bmp(out, &rotated_img) != WRITE_OK) {
        fprintf(stdout, "can't write image\n");
        close_files_n_clear_mem(in, out, &rotated_img);
        return -1;
    }

    printf("image written\n");

    if (close_file(&out) == EOF) {
        fprintf(stdout, "error occured, exiting\n");
        fprintf(stderr, "can't close file\n");
        close_files_n_clear_mem(in, out, &rotated_img);
        return -1;
    }
    image_destroy(&rotated_img);
    printf("file closed\n");

    return 0;
}
