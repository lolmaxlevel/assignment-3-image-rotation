#include "image.h"
#include "rotate.h"
#include <inttypes.h>

struct image rotate(struct image const source) {
	struct image rotated_img = image_create(source.height, source.width);
	for (uint32_t i = 0; i < source.height; i++) {
		for (uint32_t j = 0; j < source.width; j++) {
			rotated_img.data[source.height * j + source.height - i - 1] = source.data[source.width * i + j];
		}
	}
	return rotated_img;
}
