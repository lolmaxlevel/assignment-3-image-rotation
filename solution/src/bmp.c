#include "bmp.h"
#include "image.h"
//https://stackoverflow.com/questions/17550545/bmp-image-header-bixpelspermeter
//https://www.programmersought.com/article/58184894748/#bmp_1
#define BMP_TYPE 19778
#define BM_RESERVED 0
#define BOFFBITS 54
#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BICOMPRESSION 0
#define BIXPELSPERMETER 2835
#define BIYPELSPERMETER 2835
#define BICLRUSED 0
#define BICLRIMPORTANT 0


static uint8_t get_bmp_padding(uint64_t width) {
    return (width % 4 == 0) ? 0 : (4 - (width * sizeof(struct pixel)) % 4);
}


enum read_status from_bmp(FILE* in, struct image *img) {
    struct bmp_header header = { 0 };

    if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_TYPE) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != BIBITCOUNT) {
        return READ_INVALID_BITS;
    }
    *img = image_create(header.biWidth, header.biHeight);
    if (img->data == NULL) {
        return NOT_ENOUGH_MEMORY;
    }

    uint8_t padding = get_bmp_padding(img->width);
    for (size_t i = 0; i < header.biHeight; i++)
    {
        if (fread(img->data + i * header.biWidth, sizeof(struct pixel), header.biWidth, in) != header.biWidth)
            return READ_BAD_DATA;
        if (fseek(in, padding, SEEK_CUR) != 0) return READ_BAD_DATA;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = {
        .bfType = BMP_TYPE,
        .bfileSize = (sizeof(struct pixel) * img->height * (img->width + get_bmp_padding(img->width))) + sizeof(struct bmp_header),
        .bfReserved = BM_RESERVED,
        .bOffBits = BOFFBITS,
        .biSize = BISIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = BIPLANES,
        .biBitCount = BIBITCOUNT,
        .biCompression = BICOMPRESSION,
        .biSizeImage = sizeof(struct pixel) * img->height * (img->width + get_bmp_padding(img->width)),
        .biXPelsPerMeter = BIXPELSPERMETER,
        .biYPelsPerMeter = BIYPELSPERMETER,
        .biClrUsed = BICLRUSED,
        .biClrImportant = BICLRIMPORTANT
    };
    uint8_t padding = get_bmp_padding(img->width);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out)!=1)return WRITE_ERROR;

    for (size_t i = 0; i < img->height; i++)
    {
        if(fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out) != img->width) return WRITE_ERROR;
        if(fwrite("\0\0", 1, padding, out) != padding) return WRITE_ERROR;
    }
    return WRITE_OK;
}
